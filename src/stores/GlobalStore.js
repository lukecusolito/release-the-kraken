import Reflux from 'reflux';
import GlobalActions from '../actions/GlobalActions';

export default class GlobalStore extends Reflux.Store {
    constructor() {
        super();
        this.state = {
            loadingSpinnerIsActive: false,
            notification: {
                message: '',
                isActive: false,
                allowUndo: false,
                onClose: () => { },
                onUndo: () => { },
            },
            error: {
                isActive: false,
                message: ''
            }
        }
        this.listenables = [GlobalActions];
    }

    toggleLoadingSpinner(isActive) {
        this.setState({ loadingSpinnerIsActive: isActive });
    }

    showNotification(message, allowUndo, onClose, onUndo) {
        this.setState({
            notification: {
                message: message,
                isActive: true,
                allowUndo: allowUndo,
                onClose: () => { GlobalActions.hideNotification(onClose) },
                onUndo: () => { GlobalActions.hideNotification(onUndo) }
            }
        });
    }

    hideNotification(postNotificationCallback = () => { }) {
        const blankFunction = () => { };

        this.setState({
            notification: {
                message: '',
                isActive: false,
                allowUndo: false,
                onClose: blankFunction,
                onUndo: blankFunction
            }
        });

        postNotificationCallback();
    }

    toggleError(isActive, message = 'User error - Replace user') {
        this.setState({
            error: {
                isActive: isActive,
                message: message
            }
        });
        GlobalActions.toggleLoadingSpinner(false);
    }
}