import Reflux from 'reflux';
import SetlistActions from '../actions/SetlistActions';
import GlobalActions from '../actions/GlobalActions';
import api from '../data/api';
import ChangeCase from 'change-case';

export default class SetlistStore extends Reflux.Store {
    constructor() {
        super();
        this.state = {
            setlists: [],
            setlistTableSelectedRows: [],
            setlistContext: '',
            previousSetlists: [],
            previousSetlist: {},
            setlistChangesMade: false,
            trackToAdd: {
                TrackName: '',
                Artist: '',
                Duration: ''
            },
            trackToAddTrackNameError: '',
            trackToAddArtistError: '',
            trackToAddDurationError: '',
            createSetlist: {
                isActive: false,
                inputValidated: false,
                inputName: '',
                inputDescription: '',
                inputNameError: '',
                inputDescriptionError: ''
            },
            editModeIsActive: false
        }
        this.listenables = [SetlistActions];
    }

    loadSetlists(enableSpinner = true) {
        if (enableSpinner) {
            GlobalActions.toggleLoadingSpinner(true);
        }
        api.Get('/api/setlist', {}, (resp) => {
            this.setState({ setlists: resp.data, setlistChangesMade: false });
            GlobalActions.toggleLoadingSpinner(false);
        });
    }

    submitSetlistDeletion(id) {
        api.Delete('/api/setlist/' + id, {}, (resp) => { });
    }

    modifySetlist(id, updatedSetlist) {
        var options = {};
        options.requestBody = updatedSetlist;

        api.Put('/api/setlist/' + id, options, (resp) => {
            SetlistActions.loadSetlists(false);
        });
    }

    submitSetlistCreation(newSetlist) {
        GlobalActions.toggleLoadingSpinner(true);

        var options = {};
        options.requestBody = newSetlist;

        api.Post('/api/setlist', options, (resp) => {
            SetlistActions.loadSetlists();
        });
    }

    handleSetlistTableRowSelect(event, id) {
        const { setlistTableSelectedRows } = this.state;
        const selectedIndex = setlistTableSelectedRows.indexOf(id);
        var newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(setlistTableSelectedRows, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(setlistTableSelectedRows.slice(1));
        } else if (selectedIndex === setlistTableSelectedRows.length - 1) {
            newSelected = newSelected.concat(setlistTableSelectedRows.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                setlistTableSelectedRows.slice(0, selectedIndex),
                setlistTableSelectedRows.slice(selectedIndex + 1),
            );
        }

        this.setState({ setlistTableSelectedRows: newSelected });
    }

    handleSetlistTableRowSelectAll(event, checked) {
        var newSelected = []
        var index = this.state.setlists.findIndex(x => x._id === this.state.setlistContext);
        if (checked) {
            newSelected = this.state.setlists[index].Setlist.map(n => n._id);
        }
        this.setState({ setlistTableSelectedRows: newSelected });
    }

    handleSetlistPopupOpen(id) {
        var setlistContext = id;
        var { setlists } = this.state;
        var index = setlists.findIndex(x => x._id === setlistContext);

        var previousSetlist = JSON.parse(JSON.stringify(setlists[index]));

        this.setState({ setlistContext, previousSetlist });
    }

    handleSetlistPopupClose() {
        var { setlistChangesMade, editModeIsActive } = this.state;
        if (setlistChangesMade && editModeIsActive) {
            alert("TODO: Changes will be lost are you sure?");
        }

        this.setState({
            setlistContext: '',
            setlistTableSelectedRows: [],
            previousSetlist: {},
            setlistChangesMade: false,
            trackToAdd: {
                TrackName: '',
                Artist: '',
                Duration: ''
            },
            trackToAddTrackNameError: '',
            trackToAddArtistError: '',
            trackToAddDurationError: '',
            editModeIsActive: false
        });

        // Temp until confirmation dialog complete
        SetlistActions.loadSetlists(false);
    }

    removeSelectedTracksFromSetlist() {
        var setlistIndex = this.state.setlists.findIndex(x => x._id === this.state.setlistContext);
        var setlists = this.state.setlists;
        var newSetlistState = JSON.parse(JSON.stringify(setlists[setlistIndex]));
        var selectedTracks = this.state.setlistTableSelectedRows;

        var findIndex = (array, id) => { return array.findIndex(x => x._id === id); };

        for (var i = 0; i < selectedTracks.length; i++) {
            var selectedTrackIndex = findIndex(newSetlistState.Setlist, selectedTracks[i]);
            if (selectedTrackIndex > -1) {
                newSetlistState.Setlist.splice(selectedTrackIndex, 1);
            }
        }

        setlists[setlistIndex] = newSetlistState;

        this.setState({ setlists, setlistTableSelectedRows: [], setlistChangesMade: true });
    }

    undoSetlistChanges() {
        var { previousSetlist, setlists, setlistContext } = this.state;
        var setlistIndex = setlists.findIndex(x => x._id === setlistContext);
        var setlistChangesMade = false;

        setlists[setlistIndex] = previousSetlist;

        this.setState({ setlists, setlistChangesMade: setlistChangesMade });

        SetlistActions.modifySetlist(setlistContext, previousSetlist);
    }

    undoSetlistDeletion() {
        var { previousSetlists, setlists } = this.state;
        var setlistChangesMade = false;

        setlists = previousSetlists;

        this.setState({ setlists, setlistChangesMade: setlistChangesMade });
    }

    newTrackInput(event) {
        var newValue = event.target.value;
        var { trackToAdd } = this.state;

        switch (event.target.id) {
            case 'txtTrackName':
                trackToAdd.TrackName = newValue;
                break;
            case 'txtArtist':
                trackToAdd.Artist = newValue;
                break;
            case 'txtDuration':
                trackToAdd.Duration = newValue;
                break;
            default:
                break;

        }

        this.setState({ trackToAdd });

        SetlistActions.validateNewTrackInput(event);
    }

    validateNewTrackInput(event) {
        var newValue = event.target.value;
        var blankValueMessage = 'Value must be set';

        switch (event.target.id) {
            case 'txtTrackName':
                if (newValue.length === 0) {
                    this.setState({ trackToAddTrackNameError: blankValueMessage });
                } else if (this.state.trackToAddTrackNameError.length > 0) {
                    this.setState({ trackToAddTrackNameError: '' });
                }
                break;
            case 'txtArtist':
                if (newValue.length === 0) {
                    this.setState({ trackToAddArtistError: blankValueMessage });
                } else if (this.state.trackToAddArtistError.length > 0) {
                    this.setState({ trackToAddArtistError: '' });
                }
                break;
            case 'txtDuration':
                if (newValue.length === 0) {
                    this.setState({ trackToAddDurationError: blankValueMessage });
                } else if (this.state.trackToAddDurationError.length > 0) {
                    this.setState({ trackToAddDurationError: '' });
                }
                break;
            default:
                break;

        }
    }

    validateNewTrackSubmission() {
        var { trackToAddTrackNameError, trackToAddArtistError, trackToAddDurationError, setlists, trackToAdd, setlistContext } = this.state;
        var setlistIndex = setlists.findIndex(x => x._id === setlistContext);

        if (trackToAddTrackNameError.length === 0 &&
            trackToAddArtistError.length === 0 &&
            trackToAddDurationError.length === 0 &&
            trackToAdd.TrackName.length !== 0 &&
            trackToAdd.Artist.length !== 0 &&
            trackToAdd.Duration.length !== 0) {
            trackToAdd._id = Math.random();
            trackToAdd.TrackName = ChangeCase.titleCase(trackToAdd.TrackName);
            trackToAdd.Artist = ChangeCase.titleCase(trackToAdd.Artist);

            setlists[setlistIndex].Setlist.push(trackToAdd);

            this.setState({ setlists, setlistChangesMade: true });
            SetlistActions.cleanTrackToAdd();
        } else {
            GlobalActions.toggleError("Fields incomplete");
        }
    }

    cleanTrackToAdd() {
        this.setState({
            trackToAddTrackNameError: '',
            trackToAddArtistError: '',
            trackToAddDurationError: '',
            trackToAdd: {
                TrackName: '',
                Artist: '',
                Duration: ''
            }
        });
    }

    moveSetlistTrack(event, indexToMove) {
        event.stopPropagation();
        var indexToMoveTo = 0;
        var { setlists, setlistContext } = this.state;
        var setlistIndex = setlists.findIndex(x => x._id === setlistContext);

        switch (event.target.id) {
            case 'asc':
                indexToMoveTo = indexToMove--;
                break;
            case 'desc':
                indexToMoveTo = indexToMove++;
                break;
            default:
                break;
        }
        setlists[setlistIndex].Setlist.splice(indexToMoveTo, 0, setlists[setlistIndex].Setlist.splice(indexToMove, 1)[0]);

        this.setState({ setlists, setlistChangesMade: true });
    }

    submitChangesMadeToSetlist() {
        var { setlists, setlistContext } = this.state;
        var setlistIndex = setlists.findIndex(x => x._id === setlistContext);
        SetlistActions.modifySetlist(setlistContext, setlists[setlistIndex]);


        GlobalActions.showNotification(
            `Changes saved.`,
            true,
            () => { },
            () => SetlistActions.undoSetlistChanges()
        );
    }

    handleCreateSetlistPopupOpen() {
        var { createSetlist } = this.state;
        createSetlist.isActive = true;
        this.setState({ createSetlist });
    }

    handleCreateSetlistPopupClose() {
        var createSetlist = {
            isActive: false,
            inputValidated: false,
            inputName: '',
            inputDescription: '',
            inputNameError: '',
            inputDescriptionError: ''
        }

        this.setState({ createSetlist });
    }

    handleCreateSetlistTextChange(event) {
        var newValue = event.target.value;
        var blankValueMessage = 'Value must be set';
        var { createSetlist } = this.state;

        switch (event.target.id) {
            case 'txtSetlistName':
                if (newValue.length <= 30) {
                    createSetlist.inputName = newValue;
                    if (newValue.length === 0) {
                        createSetlist.inputNameError = blankValueMessage;
                    } else {
                        createSetlist.inputNameError = '';
                    }
                }
                break;
            case 'txtDescription':
                if (newValue.length <= 70) {
                    createSetlist.inputDescription = newValue;
                    if (newValue.length === 0) {
                        createSetlist.inputDescriptionError = blankValueMessage;
                    } else {
                        createSetlist.inputDescriptionError = '';
                    }
                }
                break;
            default:
                break;
        }

        if (createSetlist.inputName.length === 0 || createSetlist.inputDescription.length === 0) {
            createSetlist.inputValidated = false;
        } else {
            createSetlist.inputValidated = true;
        }

        this.setState({ createSetlist });
    }

    createSetlist() {
        var { createSetlist } = this.state;

        if (createSetlist.inputValidated) {
            var newSetlist = {
                Title: ChangeCase.titleCase(createSetlist.inputName),
                Description: ChangeCase.ucFirst(createSetlist.inputDescription),
                Setlist: []
            }

            SetlistActions.submitSetlistCreation(newSetlist);
            SetlistActions.handleCreateSetlistPopupClose();
        }
    }

    deleteSetlist(id) {
        var { setlists } = this.state;
        var setlistIndex = setlists.findIndex(x => x._id === id);
        var setlistName = setlists[setlistIndex].Title;

        var previousSetlistState = JSON.parse(JSON.stringify(setlists));

        setlists.splice(setlistIndex, 1);

        GlobalActions.showNotification(
            `Setlist '${setlistName}' removed`,
            true,
            () => SetlistActions.submitSetlistDeletion(id),
            () => SetlistActions.undoSetlistDeletion()
        );

        this.setState({ setlists, previousSetlists: previousSetlistState, setlistChangesMade: true });
    }

    handleSetlistPopupEditModeOpen(id) {
        var { setlists } = this.state;
        var setlistIndex = setlists.findIndex(x => x._id === id);

        this.setState({ editModeIsActive: true, previousSetlist: setlists[setlistIndex] });
        SetlistActions.handleSetlistPopupOpen(id);
    }
}