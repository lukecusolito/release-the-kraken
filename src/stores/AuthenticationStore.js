import Reflux from 'reflux';
import AuthenticationActions from '../actions/AuthenticationActions';

export default class AuthenticationStore extends Reflux.Store {
    constructor() {
        super();
        this.state = {
            userToAuthenticate: {
                username: '',
                password: ''
            },
            usernameFieldError: '',
            passwordFieldError: ''
        }
        this.listenables = [AuthenticationActions];
    }

    handleAuthenticationTextChange(event) {
        var newValue = event.target.value;
        var { userToAuthenticate } = this.state;

        switch (event.target.id) {
            case 'txtUsername':
                userToAuthenticate.username = newValue;
                this.setState({usernameFieldError: ''});
                break;
            case 'txtPassword':
                userToAuthenticate.password = newValue;
                this.setState({passwordFieldError: ''});
                break;
            default:
                break;

        }

        this.setState({ userToAuthenticate });
    }

    handleLoginClick() {
        var { userToAuthenticate } = this.state;
        var blankFieldError = "Field Required!";

        var usernameFieldError = userToAuthenticate.username.length !== 0 ? '' : blankFieldError;
        var passwordFieldError = userToAuthenticate.password.length !== 0 ? '' : blankFieldError;

        this.setState({ usernameFieldError, passwordFieldError });

        if (usernameFieldError.length === 0 &&
            passwordFieldError.length === 0 &&
            userToAuthenticate.username.length !== 0 &&
            userToAuthenticate.password.length !== 0) {
            alert("TODO: Call api to authenticate");
        }
    }
}