import axios from 'axios';
import config from '../config/config';
import GlobalActions from '../actions/GlobalActions';

const headerValues = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};

export const Get = (api, options, callback) => {
    axios({
        method: 'get',
        url: config.apiAddress + api,
        headers: headerValues
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        GlobalActions.toggleError(true, err.response.data.message);
    });
}

export const Delete = (api, options, callback) => {
    axios({
        method: 'delete',
        url: config.apiAddress + api,
        headers: headerValues
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        GlobalActions.toggleError(true, err.response.data.message);
    });
}

export const Put = (api, options, callback) => {
    axios({
        method: 'put',
        url: config.apiAddress + api,
        headers: headerValues,
        data: options.requestBody
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        GlobalActions.toggleError(true, err.response.data.message);
    });
}

export const Post = (api, options, callback) => {
    axios({
        method: 'post',
        url: config.apiAddress + api,
        headers: headerValues,
        data: options.requestBody
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        GlobalActions.toggleError(true, err.response.data.message);
    });
}

export default { Get, Delete, Put, Post };