import React from 'react';
import Reflux from 'reflux';
import './App.css';
import Header from './components/global/Header';
import ErrorPopup from './components/global/ErrorPopup';
import LoadingSpinner from './components/global/LoadingSpinner';
import GlobalStore from './stores/GlobalStore';
import Notification from './components/global/Notification';

class App extends Reflux.Component {
  constructor(props) {
    super(props);
    this.stores = [GlobalStore];
  }

  componentWillMount() {
    Reflux.Component.prototype.componentWillMount.call(this);
    this.props.history.push('/authentication');
  }

  render() {
    return (
      <div>
        <Header />
        {this.props.children}
        <LoadingSpinner isActive={this.state.loadingSpinnerIsActive} />
        <Notification data={this.state.notification} />
        <ErrorPopup data={this.state.error} />
      </div>
    );
  }
}

export default App;