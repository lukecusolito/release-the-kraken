import React from 'react';
import { TextField } from 'material-ui';

const SetlistTitleInputFields = ({ data, onChange }) => (
    <div>
        <div style={{ height: 55 }}>
            <TextField
                id='txtSetlistName'
                error={data.inputNameError.length > 0}
                label='Setlist Name'
                value={data.inputName}
                helperText={data.inputNameError}
                onChange={onChange}
            />
        </div>
        <br />
        <div style={{ height: 55 }}>
            <TextField
                id='txtDescription'
                error={data.inputDescriptionError.length > 0}
                label='Description'
                value={data.inputDescription}
                helperText={data.inputDescriptionError}
                onChange={onChange}
            />
        </div>
    </div>
);

export default SetlistTitleInputFields;