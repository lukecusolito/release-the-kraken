import React from 'react';
import { TableCell, TableHead, TableRow, Checkbox } from 'material-ui';
import SetlistActions from '../../../actions/SetlistActions';

const columnData = [
    { id: 'trackName', numeric: false, disablePadding: false, label: 'Track Name', editModeOnly: false },
    { id: 'artist', numeric: false, disablePadding: false, label: 'Artist', editModeOnly: false },
    { id: 'duration', numeric: false, disablePadding: false, label: 'Duration', editModeOnly: false },
    { id: 'Sorting', numeric: false, disablePadding: true, label: '', editModeOnly: true }
];

const SetlistTableHeader = ({ dataLength, selectedLength, editModeIsActive }) => (
    <TableHead>
        <TableRow>
            {editModeIsActive && <TableCell padding="checkbox">
                <Checkbox
                    indeterminate={selectedLength > 0 && selectedLength < dataLength}
                    checked={dataLength !== 0 && selectedLength === dataLength}
                    onChange={SetlistActions.handleSetlistTableRowSelectAll}
                />
            </TableCell>}
            {columnData.map(column => (
                (!column.editModeOnly || editModeIsActive === column.editModeOnly) && <TableCell
                    key={column.id}
                    numeric={column.numeric}
                    padding={column.disablePadding ? 'none' : 'default'}
                >
                    {column.label}
                </TableCell>)
            )}
        </TableRow>
    </TableHead>
);

export default SetlistTableHeader;