import React from 'react';
import { TableBody, TableCell, TableRow, Checkbox, IconButton } from 'material-ui';
import { ArrowUpward, ArrowDownward } from 'material-ui-icons';
import SetlistActions from '../../../actions/SetlistActions';
import InputCell from './SetlistTableBody/InputCell';

function checkForEnter(event) {
    if (event.charCode === 13) {
        event.preventDefault();
        event.stopPropagation();

        SetlistActions.validateNewTrackSubmission();
    }
}

const SetlistTableBody = ({ data, selected = [], trackToAddTrackNameError, trackToAddArtistError, trackToAddDurationError, trackToAdd, editModeIsActive }) => (
    <TableBody>
        {data.map((row, index, { length }) => {
            const isSelected = selected.indexOf(row._id) !== -1;
            return (
                <TableRow
                    hover
                    key={row._id}
                    onClick={event => editModeIsActive && SetlistActions.handleSetlistTableRowSelect(event, row._id)}
                    selected={isSelected}
                >
                    {editModeIsActive && <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                    </TableCell>}
                    <TableCell>{row.TrackName}</TableCell>
                    <TableCell>{row.Artist}</TableCell>
                    <TableCell>{row.Duration}</TableCell>
                    {editModeIsActive && <TableCell padding='none'>
                        <IconButton disabled={index === 0}>
                            <ArrowUpward id='asc' onClick={(event) => SetlistActions.moveSetlistTrack(event, index)} />
                        </IconButton>
                        <IconButton disabled={index === length - 1}>
                            <ArrowDownward id='desc' onClick={(event) => SetlistActions.moveSetlistTrack(event, index)} />
                        </IconButton>
                    </TableCell>}
                </TableRow>
            );
        })}
        {editModeIsActive && <TableRow>
            <TableCell padding="checkbox"></TableCell>
            <InputCell id="txtTrackName" label="Track Name" value={trackToAdd.TrackName} errorMessage={trackToAddTrackNameError} onKeyPress={checkForEnter} onChange={SetlistActions.newTrackInput} />
            <InputCell id="txtArtist" label="Artist" value={trackToAdd.Artist} errorMessage={trackToAddArtistError} onKeyPress={checkForEnter} onChange={SetlistActions.newTrackInput} />
            <InputCell id="txtDuration" label="Duration" value={trackToAdd.Duration} errorMessage={trackToAddDurationError} onKeyPress={checkForEnter} onChange={SetlistActions.newTrackInput} />
            <TableCell></TableCell>
        </TableRow>}
    </TableBody>
);

export default SetlistTableBody;