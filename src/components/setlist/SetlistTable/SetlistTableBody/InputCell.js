import React from 'react';
import { TableCell, TextField } from 'material-ui';

const InputCell = ({ id, label, value, errorMessage = '', onKeyPress, onChange }) => (
    <TableCell>
        <div style={{ height: 90 }}>
            <TextField
                error={errorMessage.length > 0}
                id={id}
                label={label}
                value={value}
                onKeyPress={onKeyPress}
                helperText={errorMessage}
                onChange={onChange}
                margin='normal'
            />
        </div>
    </TableCell>
);

export default InputCell;