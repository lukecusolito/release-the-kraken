import React from 'react';
import { Toolbar, Typography } from 'material-ui';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';
import SetlistActions from '../../../actions/SetlistActions';

const SetlistTableTitle = ({ selectedLength }) => (
    <Toolbar style={{ backgroundColor: selectedLength > 0 ? '#ff80ab' : '', borderTopLeftRadius: 2, borderTopRightRadius: 2 }}>
        <div style={{ float: 'left' }}>
            {selectedLength > 0 ? (
                <Typography type="subheading">{selectedLength} selected</Typography>
            ) : (
                    <Typography type="title">Setlist Tracks:</Typography>
                )}
        </div>
        <div style={{ float: 'right', right: 10, position: 'absolute' }}>
            {selectedLength > 0 &&
                <IconButton aria-label="Delete">
                    <DeleteIcon onClick={() => SetlistActions.removeSelectedTracksFromSetlist()} />
                </IconButton>
            }
        </div>
    </Toolbar>
);

export default SetlistTableTitle;