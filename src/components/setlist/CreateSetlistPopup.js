import React from 'react';
import { Dialog, Button, DialogTitle, DialogContent, DialogActions } from 'material-ui';
import SetlistActions from '../../actions/SetlistActions';
import SetlistTitleInputFields from './CreateSetlistPopup/SetlistTitleInputFields';

const CreateSetlistPopup = ({ data }) => (
    <div style={{ float: 'left' }}>
        <Dialog
            onRequestClose={() => SetlistActions.handleCreateSetlistPopupClose()}
            open={data.isActive}
            maxWidth='md'
        >
            <DialogTitle>Create New Setlist</DialogTitle>
            <DialogContent>
                <SetlistTitleInputFields
                    data={data}
                    onChange={SetlistActions.handleCreateSetlistTextChange}
                />
            </DialogContent>
            <DialogActions>
                {data.inputValidated && <Button onClick={() => SetlistActions.createSetlist()} color="accent">
                    Add
                </Button>}
                <Button onClick={() => SetlistActions.handleCreateSetlistPopupClose()} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    </div>
);

export default CreateSetlistPopup;