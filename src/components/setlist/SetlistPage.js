import React from 'react';
import Reflux from 'reflux';
import SetlistStore from '../../stores/SetlistStore';
import SetlistWidget from './SetlistWidget';
import AddSetlistButton from './AddSetlistButton';
import SetlistActions from '../../actions/SetlistActions';

export default class SetlistPage extends Reflux.Component {
    constructor(props) {
        super(props);
        this.stores = [SetlistStore];
    }

    componentWillMount() {
        Reflux.Component.prototype.componentWillMount.call(this);
        SetlistActions.loadSetlists();
    }

    render() {
        return (
            <div style={{ width: '60%', margin: '0 auto', paddingTop: 10 }}>
                {this.state.setlists && this.state.setlists.length > 0 && this.state.setlists.map((setlist, index) =>
                    <SetlistWidget
                        key={index}
                        id={setlist._id}
                        title={setlist.Title}
                        description={setlist.Description}
                        data={setlist.Setlist}
                        tableRowsSelected={this.state.setlistTableSelectedRows}
                        trackToAddTrackNameError={this.state.trackToAddTrackNameError}
                        trackToAddArtistError={this.state.trackToAddArtistError}
                        trackToAddDurationError={this.state.trackToAddDurationError}
                        trackToAdd={this.state.trackToAdd}
                        changesMade={this.state.setlistChangesMade}
                        editModeIsActive={this.state.editModeIsActive}
                    />
                )}
                <AddSetlistButton createSetlistData={this.state.createSetlist} />
            </div>
        );
    }
}