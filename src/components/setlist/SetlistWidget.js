import React from 'react';
import { Paper, Typography, IconButton, Menu, MenuItem } from 'material-ui';
import MoreVertIcon from 'material-ui-icons/MoreVert';
import SetlistPopup from './SetlistPopup';
import SetlistActions from '../../actions/SetlistActions';

export default class SetlistWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            popupIsActive: false,
            menuIsActive: false,
            menuAnchorEl: null
        }
    }

    togglePopup(isActive, id, editMode = false) {
        if (isActive) {
            if (editMode) {
                SetlistActions.handleSetlistPopupEditModeOpen(id);
                this.setState({ menuIsActive: false });
            } else {
                SetlistActions.handleSetlistPopupOpen(id);
            }
        } else {
            SetlistActions.handleSetlistPopupClose();
        }

        this.setState({ popupIsActive: isActive })
    }

    toggleMenu(event) {
        event.stopPropagation();
        var currentState = this.state.menuIsActive;
        this.setState({ menuIsActive: !currentState, menuAnchorEl: event.currentTarget });
    }

    deleteSetlist(event) {
        event.stopPropagation();
        this.setState({ menuIsActive: false, menuAnchorEl: null });
        SetlistActions.deleteSetlist(this.props.id);
    }

    render() {
        const menuItems = [
            <MenuItem key={'widget-menu-edit'} onClick={() => this.togglePopup(true, this.props.id, true)}>Edit</MenuItem>,
            <MenuItem key={'widget-menu-delete'} onClick={(event) => this.deleteSetlist(event)}>Delete</MenuItem>
        ];
        return (
            <div style={{ float: 'left' }}>
                <Paper
                    onClick={() => this.togglePopup(true, this.props.id)}
                    title={this.props.title}
                    style={{ width: 200, height: 200, textAlign: 'center', cursor: 'pointer', margin: 5 }}
                >
                    <div style={{ height: '100%', width: '100%', position: 'relative' }}>
                        <Typography type="headline" component="h3" style={{ overflow: 'hidden', textOverflow: 'ellipsis', wordWrap: 'break-word' }}>
                            {this.props.title}
                        </Typography>
                        <Typography type="body1" component="p" style={{ overflow: 'hidden', textOverflow: 'ellipsis', wordWrap: 'break-word' }}>
                            {this.props.description}
                        </Typography>

                        <div style={{ position: 'absolute', bottom: 0, right: 0 }}>
                            <IconButton
                                onClick={this.toggleMenu.bind(this)}
                            >
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                anchorEl={this.state.menuAnchorEl}
                                open={this.state.menuIsActive}
                                onRequestClose={this.toggleMenu.bind(this)}
                            >
                                {menuItems}
                            </Menu>
                        </div>
                    </div>
                </Paper>
                <SetlistPopup
                    isActive={this.state.popupIsActive}
                    title={this.props.title}
                    data={this.props.data}
                    handleClose={() => this.togglePopup(false, this.props.id)}
                    tableRowsSelected={this.props.tableRowsSelected}
                    trackToAddTrackNameError={this.props.trackToAddTrackNameError}
                    trackToAddArtistError={this.props.trackToAddArtistError}
                    trackToAddDurationError={this.props.trackToAddDurationError}
                    trackToAdd={this.props.trackToAdd}
                    changesMade={this.props.changesMade}
                    editModeIsActive={this.props.editModeIsActive} />
            </div>
        );
    }
}