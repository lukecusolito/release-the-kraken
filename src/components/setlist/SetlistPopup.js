import React from 'react';
import { Dialog, Button, DialogTitle, DialogContent, DialogActions } from 'material-ui';
import SetlistTable from './SetlistTable';
import SetlistActions from '../../actions/SetlistActions';

const SetlistPopup = (props) => (
    <div style={{ float: 'left' }}>
        <Dialog
            onRequestClose={props.handleClose}
            open={props.isActive}
            maxWidth='md'
        >
            <DialogTitle>{props.title}</DialogTitle>
            <DialogContent>
                <SetlistTable
                    data={props.data}
                    selected={props.tableRowsSelected}
                    trackToAddTrackNameError={props.trackToAddTrackNameError}
                    trackToAddArtistError={props.trackToAddArtistError}
                    trackToAddDurationError={props.trackToAddDurationError}
                    trackToAdd={props.trackToAdd}
                    editModeIsActive={props.editModeIsActive}
                />
            </DialogContent>
            <DialogActions>
                {props.editModeIsActive && props.changesMade && <Button onClick={() => SetlistActions.submitChangesMadeToSetlist()} color="accent">
                    Save
                </Button>}
                <Button onClick={props.handleClose} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    </div>
);

export default SetlistPopup;