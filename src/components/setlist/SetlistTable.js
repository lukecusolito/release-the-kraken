import React from 'react';
import { Table, Paper } from 'material-ui';
import SetlistTableHeader from './SetlistTable/SetlistTableHeader';
import SetlistTableTitle from './SetlistTable/SetlistTableTitle';
import SetlistTableBody from './SetlistTable/SetlistTableBody';

const SetlistTable = ({ data, selected = [], trackToAddTrackNameError, trackToAddArtistError, trackToAddDurationError, trackToAdd, editModeIsActive }) => (
    <div>
        <br />
        <Paper>
            <SetlistTableTitle
                selectedLength={selected.length}
            />
            <Table>
                <SetlistTableHeader
                    dataLength={data.length}
                    selectedLength={selected.length}
                    editModeIsActive={editModeIsActive}
                />
                <SetlistTableBody
                    data={data}
                    selected={selected}
                    trackToAddTrackNameError={trackToAddTrackNameError}
                    trackToAddArtistError={trackToAddArtistError}
                    trackToAddDurationError={trackToAddDurationError}
                    trackToAdd={trackToAdd}
                    editModeIsActive={editModeIsActive}
                />
            </Table>
            <br />
        </Paper>
    </div>
);

export default SetlistTable;