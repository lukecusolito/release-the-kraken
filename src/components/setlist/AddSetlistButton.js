import React from 'react';
import { Button } from 'material-ui';
import AddIcon from 'material-ui-icons/Add';
import CreateSetlistPopup from './CreateSetlistPopup';
import SetlistActions from '../../actions/SetlistActions';

const AddSetlistButton = ({ createSetlistData }) => (
    <div style={{ float: 'left', height: 200, position: 'relative', margin: 5 }}>
        <div style={{ bottom: 0, position: 'absolute' }} >
            <Button fab color="primary" onClick={() => SetlistActions.handleCreateSetlistPopupOpen()} >
                <AddIcon />
            </Button>
        </div>
        <CreateSetlistPopup data={createSetlistData} />
    </div >
);

export default AddSetlistButton;