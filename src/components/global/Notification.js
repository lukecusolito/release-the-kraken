import React from 'react';
import { Snackbar, Button, IconButton } from 'material-ui'
import CloseIcon from 'material-ui-icons/Close';

const Notification = ({ data }) => (
    <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left', }}
        open={data.isActive}
        autoHideDuration={6000}
        onRequestClose={data.onClose}
        SnackbarContentProps={{ 'aria-describedby': 'message-id', }}
        message={<span id="message-id">{data.message}</span>}
        action={[
            <div key={"undo"}>{data.allowUndo && <Button color="accent" dense onClick={data.onUndo}>
                UNDO
            </Button>}</div>,
            <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={data.onClose}
            >
                <CloseIcon />
            </IconButton>,
        ]}
    />
);

export default Notification;