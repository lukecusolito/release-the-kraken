import React from 'react';
import { AppBar, Toolbar } from 'material-ui';
import Typography from 'material-ui/Typography';
import icon from '../../img/hydra2.png';

const appName = "Release The Kraken";

const Header = () => (
    <div>
        <AppBar position="static" title={appName}>
            <Toolbar style={{ backgroundColor: '#05263d' }}>
                <div style={{ marginRight: 10 }}>
                    <img src={icon} alt={appName} width={50} />
                </div>
                <div>
                    <Typography type="title" color="inherit">
                        {appName}
                    </Typography>
                </div>
            </Toolbar>
        </AppBar>
    </div>
);

export default Header;