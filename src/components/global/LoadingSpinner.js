import React from 'react';
import { CircularProgress } from 'material-ui';

const LoadingSpinner = ({ isActive }) => (
    <div>
        {isActive &&
            <div style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', zIndex: 100000 }}>
                <div style={{ background: 'rgba(188,188,188,0.5)', borderRadius: 10, padding: 10 }}>
                    <CircularProgress size={200} />
                </div>
            </div>
        }
    </div>
);

export default LoadingSpinner;