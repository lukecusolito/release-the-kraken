import React from 'react';
import { Dialog, Button, DialogTitle, DialogContent, DialogActions } from 'material-ui';
import GlobalActions from '../../actions/GlobalActions';

const ErrorPopup = ({ data }) => (
    <div style={{ float: 'left' }}>
        <Dialog
            onRequestClose={() => GlobalActions.toggleError(false)}
            open={data.isActive}
            maxWidth='md'
        >
            <DialogTitle>Awkward moment!</DialogTitle>
            <DialogContent>
                {data.message}
            </DialogContent>
            <DialogActions>
                <Button onClick={() => GlobalActions.toggleError(false)} color="primary">
                    Alrighty
                </Button>
            </DialogActions>
        </Dialog>
    </div>
);

export default ErrorPopup;