import React from 'react';
import Reflux from 'reflux';
import AuthenticationStore from '../../stores/AuthenticationStore';
import AuthenticationActions from '../../actions/AuthenticationActions';
import { Paper, TextField, Button } from 'material-ui';

export default class App extends Reflux.Component {
    constructor(props) {
        super(props);
        this.stores = [AuthenticationStore];
    }
    render() {
        return (
            <div style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
                <Paper style={{ width: 300, height: 300 }}>
                    <div style={{ width: '100%', height: '100%', position: 'absolute' }}>
                        <div style={{ position: 'relative', margin: 20 }}>
                            <div style={{height: 75}}>
                                <TextField
                                    id="txtUsername"
                                    label={"Username"}
                                    margin="normal"
                                    error={this.state.usernameFieldError.length !== 0}
                                    helperText={this.state.usernameFieldError}
                                    onChange={AuthenticationActions.handleAuthenticationTextChange}
                                />
                            </div>
                            <div style={{height: 75}}>
                                <TextField
                                    id="txtPassword"
                                    label={"Password"}
                                    type="password"
                                    margin="normal"
                                    error={this.state.passwordFieldError.length !== 0}
                                    helperText={this.state.passwordFieldError}
                                    onChange={AuthenticationActions.handleAuthenticationTextChange}
                                />
                            </div>
                            <br />
                            <br />
                            <div>
                                <Button raised color="primary" onClick={() => AuthenticationActions.handleLoginClick()}>Login</Button>
                            </div>
                        </div>
                    </div>
                </Paper>
            </div>
        );
    }
}