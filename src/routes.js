import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import SetlistPage from './components/setlist/SetlistPage';
import AuthenticationPage from './components/authentication/AuthenticationPage';
import App from './App';

const Routes = () => (
    <Router>
        <div className='App'>
            <Route path="/" component={App} />
            <Route exact path="/" component={SetlistPage} />
            <Route path="/authentication" component={AuthenticationPage} />
        </div>
    </Router>
)
export default Routes