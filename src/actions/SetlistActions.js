import Reflux from 'reflux';

export const loadSetlists = Reflux.createAction('loadSetlists');
export const submitSetlistDeletion = Reflux.createAction('submitSetlistDeletion');
export const modifySetlist = Reflux.createAction('modifySetlist');
export const submitSetlistCreation = Reflux.createAction('submitSetlistCreation');
export const handleSetlistTableRowSelect = Reflux.createAction('handleSetlistTableRowSelect');
export const handleSetlistTableRowSelectAll = Reflux.createAction('handleSetlistTableRowSelectAll');
export const handleSetlistPopupOpen = Reflux.createAction('handleSetlistPopupOpen');
export const handleSetlistPopupClose = Reflux.createAction('handleSetlistPopupClose');
export const removeSelectedTracksFromSetlist = Reflux.createAction('removeSelectedTracksFromSetlist');
export const undoSetlistChanges = Reflux.createAction('undoSetlistChanges');
export const validateNewTrackInput = Reflux.createAction('validateNewTrackInput');
export const newTrackInput = Reflux.createAction('newTrackInput');
export const validateNewTrackSubmission = Reflux.createAction('validateNewTrackSubmission');
export const cleanTrackToAdd = Reflux.createAction('cleanTrackToAdd');
export const moveSetlistTrack = Reflux.createAction('moveSetlistTrack');
export const submitChangesMadeToSetlist = Reflux.createAction('submitChangesMadeToSetlist');
export const handleCreateSetlistPopupOpen = Reflux.createAction('handleCreateSetlistPopupOpen');
export const handleCreateSetlistPopupClose = Reflux.createAction('handleCreateSetlistPopupClose');
export const handleCreateSetlistTextChange = Reflux.createAction('handleCreateSetlistTextChange');
export const createSetlist = Reflux.createAction('createSetlist');
export const deleteSetlist = Reflux.createAction('deleteSetlist');
export const handleSetlistPopupEditModeOpen = Reflux.createAction('handleSetlistPopupEditModeOpen');
export const undoSetlistDeletion = Reflux.createAction('undoSetlistDeletion');

export default {
    loadSetlists,
    submitSetlistDeletion,
    modifySetlist,
    submitSetlistCreation,
    handleSetlistTableRowSelect,
    handleSetlistTableRowSelectAll,
    handleSetlistPopupOpen,
    handleSetlistPopupClose,
    removeSelectedTracksFromSetlist,
    undoSetlistChanges,
    validateNewTrackInput,
    newTrackInput,
    validateNewTrackSubmission,
    cleanTrackToAdd,
    moveSetlistTrack,
    submitChangesMadeToSetlist,
    handleCreateSetlistPopupOpen,
    handleCreateSetlistPopupClose,
    handleCreateSetlistTextChange,
    createSetlist,
    deleteSetlist,
    handleSetlistPopupEditModeOpen,
    undoSetlistDeletion
};