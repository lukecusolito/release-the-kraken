import Reflux from 'reflux';

export const toggleLoadingSpinner = Reflux.createAction('toggleLoadingSpinner');
export const showNotification = Reflux.createAction('showNotification');
export const hideNotification = Reflux.createAction('hideNotification');
export const toggleError = Reflux.createAction('toggleError');

export default {
    toggleLoadingSpinner,
    showNotification,
    hideNotification,
    toggleError
};