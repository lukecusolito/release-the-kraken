import Reflux from 'reflux';

export const handleAuthenticationTextChange = Reflux.createAction('handleAuthenticationTextChange');
export const handleLoginClick = Reflux.createAction('handleLoginClick');

export default {
    handleAuthenticationTextChange,
    handleLoginClick
};